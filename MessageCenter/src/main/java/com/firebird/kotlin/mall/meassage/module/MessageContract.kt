package com.firebird.kotlin.mall.meassage.module

import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView
import com.firebird.kotlin.mall.meassage.data.Message

interface MessageContract {
    interface View : IBaseView {
        //获取消息列表回调
        fun onGetMessageResult(result:MutableList<Message>?)
    }

    interface Presenter : IBasePresenter<View>
}