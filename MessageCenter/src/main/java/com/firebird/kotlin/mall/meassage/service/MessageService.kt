package com.firebird.kotlin.mall.meassage.service

import com.firebird.kotlin.mall.meassage.data.Message
import rx.Observable

/*
  消息业务接口
 */
interface MessageService {
    //获取消息列表
    fun getMessageList(): Observable<MutableList<Message>?>
}
