package com.firebird.kotlin.mall.pay.data

/*
  支付订单
 */
data class PayOrderReq(val orderId:Int)
