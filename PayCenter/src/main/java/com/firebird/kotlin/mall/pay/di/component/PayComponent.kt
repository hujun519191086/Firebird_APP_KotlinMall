package com.firebird.kotlin.mall.pay.di.component

import com.firebird.kotlin.mall.library.di.component.ActivityComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import com.firebird.kotlin.mall.pay.di.module.PayModule
import com.firebird.kotlin.mall.pay.module.PayActivity
import dagger.Component

/*
  支付Component
 */
@ComponentScope
@Component(dependencies = arrayOf(ActivityComponent::class), modules = arrayOf(PayModule::class))
interface PayComponent {
    fun inject(activity: PayActivity)
}
