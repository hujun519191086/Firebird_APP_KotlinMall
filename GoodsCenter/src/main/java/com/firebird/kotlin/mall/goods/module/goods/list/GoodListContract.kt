package com.firebird.kotlin.mall.goods.module.goods.list

import com.firebird.kotlin.mall.goods.data.Goods
import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface GoodListContract {
    interface View : IBaseView {
        //获取商品列表
        fun onGetGoodsListResult(result: Goods)
    }

    interface Presenter : IBasePresenter<View>
}