package com.firebird.kotlin.mall.goods.module.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebird.kotlin.mall.goods.R
import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.library.mvp.adapter.BaseRecyclerViewAdapter
import kotlinx.android.synthetic.main.layout_top_category_item.view.*

/*
  一级商品分类 Adapter
 */
class TopCategoryAdapter(context: Context?) : BaseRecyclerViewAdapter<BrotherCategory, TopCategoryAdapter.ViewHolder>(context) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.layout_top_category_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)

        val model = dataList[position]
        //al brotherCategorys:List<BrotherCategory> = model.brotherCategory
        if (model.name!="root") {
            //分类名称
            holder.itemView.mTopCategoryNameTv.text = model.name
            //是否被选中
            holder.itemView.mTopCategoryNameTv.isSelected = model.isSelected
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
