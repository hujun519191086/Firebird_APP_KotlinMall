package com.firebird.kotlin.mall.goods.module.cart

import com.firebird.kotlin.mall.goods.data.CartGoods
import com.firebird.kotlin.mall.goods.data.CartInfo
import com.firebird.kotlin.mall.goods.data.DeleteCartReq
import com.firebird.kotlin.mall.goods.service.CartService
import com.firebird.kotlin.mall.library.ext.excute
import com.firebird.kotlin.mall.library.mvp.presenter.BasePresenter
import com.firebird.kotlin.mall.library.rx.BaseSubscriber
import javax.inject.Inject

/*
  购物车 Presenter
 */
class CartPresenter @Inject constructor() : BasePresenter<CartContract.View>(), CartContract.Presenter {

    @Inject
    lateinit var cartService: CartService

    /*
      获取购物车列表
     */
    fun getCartList() {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        //test:userID=1
        cartService.getCartList(1).excute(object : BaseSubscriber<MutableList<CartGoods>?>(getView()) {
            override fun onNext(t: MutableList<CartGoods>?) {
                getView().onGetCartListResult(t)
            }
        }, mLifecycleProvider)
    }

    /*
      删除购物车商品
     */
    fun deleteCartList(userId:Int,deleteCartReq: DeleteCartReq) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        cartService.deleteCartList(userId,deleteCartReq).excute(object : BaseSubscriber<CartInfo>(getView()) {
            override fun onNext(t: CartInfo) {
                getView().onDeleteCartListResult(t)
            }
        }, mLifecycleProvider)
    }

    /*
      提交购物车商品
     */
    fun submitCart(list: MutableList<CartGoods>, totalPrice: Long) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        cartService.submitCart(list, totalPrice).excute(object : BaseSubscriber<Int>(getView()) {
            override fun onNext(t: Int) {
                getView().onSubmitCartListResult(t)
            }
        }, mLifecycleProvider)
    }

    /*
      更新购物车商品
     */
    fun updateCartGoods(list: MutableList<CartGoods>) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        cartService.updateCartGoods(list).excute(object : BaseSubscriber<Int>(getView()) {
            override fun onNext(t: Int) {
                getView().onUpdateCartGoodsResult(t)
            }
        }, mLifecycleProvider)
    }
}
