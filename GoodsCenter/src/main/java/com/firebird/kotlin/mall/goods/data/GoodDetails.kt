package com.firebird.kotlin.mall.goods.data

/*
  商品数据类
 */

data class GoodDetails(
        val errno:Int,val errmsg :String,val data:GoodDetailData
)

data class  GoodDetailData(
        val specificationList:List<SpecificationList>,
        val issue:MutableList<Issue>,
        val userHasCollect:Int,
        var shareImage:String,
        val comment:GoodComment,
        val share:Boolean,
        val attribute:List<GddAttribute>,
        val productList:List<ProductList>,
        val info:GoodInfo
)
data class GddAttribute(
        val id: Int,
        val goodsId: Int,
        val attribute: String,
        val value:String,
        val addTime:String,
        val updateTime:String,
        val deleted:Boolean
)
data class SpecificationList(
        val name: String,
        val valueList:List<ValueList>
)
data class ValueList(
        val id: Int,
        val goodsId: Int,
        val specification:String,
        val value:String,
        val picUrl:String,
        val addTime:String,
        val updateTime:String,
        val deleted:Boolean
)
data class Issue(
        val id: Int,
        val question: String,
        val answer: String,
        val addTime:String,
        val updateTime:String,
        val deleted:Boolean
)
data class GoodComment(
        val data:List<String>,
        val count:Int
)
data class ProductList(
        val id:Int,val goodsId:Int,val price:Float, val number:Int,val url:String,val addTime: String,
        val updateTime: String,val deleted: Boolean
)
data class GoodInfo(
        val id: Int,val goodsSn:String,val name:String,val categoryId:Int,val brandId:Int,
        val gallery:List<String>,val keywords:String,
        val brief:String,val isOnSale:Boolean,val sortOrder:Int,val picUrl:String,val shareUrl:String,
        val isNew:Boolean,val isHot:Boolean,val unit:String,val counterPrice:Float,
        val retailPrice:Float,val addTime: String,
        val updateTime: String,val deleted: Boolean,val detail:String
)