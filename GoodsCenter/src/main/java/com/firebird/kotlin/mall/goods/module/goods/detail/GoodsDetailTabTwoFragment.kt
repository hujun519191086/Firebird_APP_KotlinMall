package com.firebird.kotlin.mall.goods.module.goods.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eightbitlab.rxbus.Bus
import com.eightbitlab.rxbus.registerInBus
import com.firebird.kotlin.mall.goods.R
import com.firebird.kotlin.mall.goods.event.GoodsDetailhtmlEvent
import com.firebird.kotlin.mall.library.mvp.view.fragment.BaseFragment
import kotlinx.android.synthetic.main.fragment_goods_detail_tab_two.*



/*
  商品详情Tab Two
 */
class GoodsDetailTabTwoFragment : BaseFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater?.inflate(R.layout.fragment_goods_detail_tab_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
/*
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });*/

        initObserve()
    }

    /*
      初始化监听，商品详情获取成功后，加载当前页面
     */
    private fun initObserve() {
        Bus.observe<GoodsDetailhtmlEvent>()
                .subscribe {
                    t: GoodsDetailhtmlEvent ->
                    run {
                        //val detailHtml = data.getDetailHtml();
                        //图片宽度改为100%  高度为自适应
                        val varjs = "<script type='text/javascript'> \nwindow.onload = function()\n{var \$img = document.getElementsByTagName('img');for(var p in  \$img){\$img[p].style.width = '100%'; \$img[p].style.height ='auto'}}</script>"

                       // mWebView.loadData(varjs + t.detailHtml, "text/html", "UTF-8")
                        mWebView.loadData(  t.detailHtml, "text/html", "UTF-8")

                    }
                }
                .registerInBus(this)

    }

    override fun onDestroy() {
        super.onDestroy()
        Bus.unregister(this)
    }
}
