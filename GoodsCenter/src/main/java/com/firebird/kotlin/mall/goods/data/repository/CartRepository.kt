package com.firebird.kotlin.mall.goods.data.repository

import com.firebird.kotlin.mall.goods.data.*
import com.firebird.kotlin.mall.goods.data.api.CartApi
import com.firebird.kotlin.mall.library.data.BaseResp
import com.firebird.kotlin.mall.library.data.net.RetrofitFactory
import rx.Observable
import javax.inject.Inject

/*
  购物车数据层
 */
class CartRepository @Inject constructor() {

    /*
      获取购物车列表
     */
    fun getCartList(userId: Int): Observable<BaseResp<MutableList<CartGoods>?>> {
        return RetrofitFactory.instance.create(CartApi::class.java).getCartList(userId)
    }

    /*
      添加商品到购物车
     */
    fun addCart(userId: Int,req: AddCartReq): Observable<Int> {
        return RetrofitFactory.instance.create(CartApi::class.java)
                .addCart(userId,req)
    }

    /*
      删除购物车商品
     */
    fun deleteCartList(userId: Int,deleteCartReq: DeleteCartReq): Observable<BaseResp<CartInfo>> {
        return RetrofitFactory.instance.create(CartApi::class.java).deleteCartList(userId,deleteCartReq)
    }

    /*
      购物车结算
     */
    fun submitCart(list: MutableList<CartGoods>, totalPrice: Long): Observable<BaseResp<Int>> {
        return RetrofitFactory.instance.create(CartApi::class.java).submitCart(SubmitCartReq(list, totalPrice))
    }

    /*
      更新购物车的数据
     */
    fun updateCartGoods(list: MutableList<CartGoods>): Observable<BaseResp<Int>> {
        return RetrofitFactory.instance.create(CartApi::class.java).updateCartGoods(UpdateGoodsListReq(list))
    }
}
