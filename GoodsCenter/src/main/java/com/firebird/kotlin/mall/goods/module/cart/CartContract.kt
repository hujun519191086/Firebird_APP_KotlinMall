package com.firebird.kotlin.mall.goods.module.cart

import com.firebird.kotlin.mall.goods.data.CartGoods
import com.firebird.kotlin.mall.goods.data.CartInfo
import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface CartContract {
    interface View : IBaseView {
        //获取购物车列表
        fun onGetCartListResult(result: MutableList<CartGoods>?)
        //删除购物车
        fun onDeleteCartListResult(result: CartInfo)
        //提交购物车
        fun onSubmitCartListResult(result: Int)
        //更新购物车
        fun onUpdateCartGoodsResult(result: Int)
    }

    interface Presenter : IBasePresenter<View> {

    }
}