package com.firebird.kotlin.mall.goods.data.repository

import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.goods.data.Category
import com.firebird.kotlin.mall.goods.data.api.CategoryApi
import com.firebird.kotlin.mall.library.data.BaseResp
import com.firebird.kotlin.mall.library.data.net.RetrofitFactory
import rx.Observable
import javax.inject.Inject

/*
    商品分类 数据层
 */
class CategoryRepository @Inject constructor(){
    /*
        获取商品分类
     */
    fun getCategory(id: Int): Observable<BaseResp<Category>> {
    //fun getCategory(id: Int): Observable<BaseResp<MutableList<Category<BrotherCategory>>>> {
        return RetrofitFactory.instance.create(CategoryApi::class.java).getCategory(id)
    }

}
