package com.firebird.kotlin.mall.goods.injection.component

import com.firebird.kotlin.mall.goods.di.module.CartModule
import com.firebird.kotlin.mall.goods.di.module.GoodsModule
import com.firebird.kotlin.mall.goods.module.goods.detail.GoodsDetailTabOneFragment
import com.firebird.kotlin.mall.library.di.component.FragmentComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import dagger.Component

/*
  商品Component
 */
@ComponentScope
@Component(dependencies = arrayOf(FragmentComponent::class), modules = arrayOf(GoodsModule::class, CartModule::class))
interface GoodsFragmentComponent {
    fun inject(fragment: GoodsDetailTabOneFragment)
}
