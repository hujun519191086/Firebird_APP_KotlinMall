package com.firebird.kotlin.mall.goods.event

/*
  商品详情Tab two 事件
 */
class GoodsDetailhtmlEvent(val detailHtml: String)
