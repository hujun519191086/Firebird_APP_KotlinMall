package com.firebird.kotlin.mall.goods.event

import com.firebird.kotlin.mall.goods.data.CartGoods

/*
  更新总价事件
 */
class UpdateTotalPriceEvent(val good: CartGoods)
