package com.firebird.kotlin.mall.goods.module.category

import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.goods.data.Category
import com.firebird.kotlin.mall.goods.service.CategoryService
import com.firebird.kotlin.mall.library.ext.excute
import com.firebird.kotlin.mall.library.mvp.presenter.BasePresenter
import com.firebird.kotlin.mall.library.rx.BaseSubscriber
import javax.inject.Inject

/*
  商品分类 Presenter
 */
class CategoryPresenter @Inject constructor() : BasePresenter<CategoryContract.View>(), CategoryContract.Presenter {

    @Inject
    lateinit var categoryService: CategoryService

    /*
      获取商品分类列表
     */
    fun getCategory(id: Int) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        categoryService.getCategory(id).excute(object : BaseSubscriber<Category>(getView()) {
            override fun onNext(t: Category) {
                getView().onGetCategoryResult(t)
            }
        }, mLifecycleProvider)
    }
}
