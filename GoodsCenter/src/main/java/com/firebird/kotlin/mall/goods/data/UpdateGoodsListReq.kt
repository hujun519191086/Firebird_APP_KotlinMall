package com.firebird.kotlin.mall.goods.data

data class UpdateGoodsListReq(val goodsList: List<CartGoods>)