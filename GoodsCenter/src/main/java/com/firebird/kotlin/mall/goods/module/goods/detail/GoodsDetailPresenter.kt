package com.firebird.kotlin.mall.goods.module.goods.detail

import com.firebird.kotlin.mall.goods.core.GoodsConstant
import com.firebird.kotlin.mall.goods.data.AddCartReq
import com.firebird.kotlin.mall.goods.data.GoodDetails
import com.firebird.kotlin.mall.goods.data.Goods
import com.firebird.kotlin.mall.goods.service.CartService
import com.firebird.kotlin.mall.goods.service.GoodsService
import com.firebird.kotlin.mall.library.ext.excute
import com.firebird.kotlin.mall.library.mvp.presenter.BasePresenter
import com.firebird.kotlin.mall.library.rx.BaseSubscriber
import com.firebird.kotlin.mall.library.util.AppPrefsUtils
import com.firebird.kotlin.mall.library.util.LogUtils
import javax.inject.Inject

/*
  商品详情 Presenter
 */
class GoodsDetailPresenter @Inject constructor() : BasePresenter<GoodsDetailContract.View>(), GoodsDetailContract.Presenter {

    @Inject
    lateinit var goodsService: GoodsService

    @Inject
    lateinit var cartService: CartService

    /*
      获取商品详情
     */
    fun getGoodsDetailList(goodsId: Int) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        goodsService.getGoodsDetail(goodsId).excute(object : BaseSubscriber<GoodDetails>(getView()) {
            override fun onNext(t: GoodDetails) {
                getView().onGetGoodsDetailResult(t)
            }
            override fun onError(e: Throwable?) {
                getView().onGetGoodsDetailError(e!!.message)
            }
        }, mLifecycleProvider)
    }

    /*
      加入购物车
     */
    fun addCart(userId:Int,addCartReq: AddCartReq) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        cartService.addCart(userId,addCartReq).excute(object : BaseSubscriber<Int>(getView()) {
            override fun onNext(t: Int) {
                LogUtils.d("商城-购物车模块-加入-回调")
                AppPrefsUtils.instant.putInt(GoodsConstant.SP_CART_SIZE, t)
                LogUtils.d("回调值：$t")
                getView().onAddCartResult(t)
            }
        }, mLifecycleProvider)
    }
}
