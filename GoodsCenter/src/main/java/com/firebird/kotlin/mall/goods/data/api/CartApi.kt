package com.firebird.kotlin.mall.goods.data.api

import com.firebird.kotlin.mall.goods.data.*
import com.firebird.kotlin.mall.library.data.BaseResp
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable


/*
  购物车接口
 */
interface CartApi {
    /*
      获取购物车列表
     */
    @POST("wx/cart/index")
    fun getCartList(@Query("userId") userId: Int): Observable<BaseResp<MutableList<CartGoods>?>>

    /*
      添加商品到购物车
     */
    //@Multipart
    @POST("wx/cart/add")
    fun addCart(@Header("userId") userId:Int, @Body req: AddCartReq): Observable<Int>
    //fun addCart(@Path("userId") userId:Int,
        //    @PartMap map: Map<String, RequestBody>):Observable<Int>
    //fun addCart(@Query("userId") userId:Int, @Body req: AddCartReq): Observable<Int>

    /*
      删除购物车商品
     */
    @POST("wx/cart/delete")
    fun deleteCartList(@Query("userId") userId: Int,@Body req: DeleteCartReq): Observable<BaseResp<CartInfo>>

    /*
      提交购物车商品
     */
    @POST("cart/submit")
    fun submitCart(@Body req: SubmitCartReq): Observable<BaseResp<Int>>

    /*
      更新购物车的数据
     */
    @POST("cart/updateCartGoods")
    fun updateCartGoods(@Body req: UpdateGoodsListReq): Observable<BaseResp<Int>>
}
