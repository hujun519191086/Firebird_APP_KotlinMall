package com.firebird.kotlin.mall.goods.data

/*
  提交购物车
 */
data class SubmitCartReq(val goodsList: List<CartGoods>, val totalPrice: Long)
