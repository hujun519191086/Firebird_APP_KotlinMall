package com.firebird.kotlin.mall.goods.data

/*
  获取商品详情请求
 */
data class GetGoodsDetailReq(val goodsId: Int)
