package com.firebird.kotlin.mall.goods.data

/*
  商品数据类
 */
data class Goods(
        val total:Int,
        val pages:Int,
        val limit:Int,
        val page:Int,
        val list:MutableList<Good>,
        val filterCategoryList:MutableList<FilterCategoryList>
)
data class Good(
        val id: Int,//商品ID
        val name: String,
        val brief: String,//商品描述
        val picUrl: String,//默认图标
        val isNew: Boolean,
        val isHot: Boolean,
        val counterPrice: Float,
        val retailPrice: Float
        )
data class FilterCategoryList(
        val id: Int,//商品ID
        val name: String,
        val keywords: String,//商品描述
        val desc: String,//默认图标
        val pid: Int,
        val iconUrl: String,
        val picUrl: String,
        val level: String,
        val sortOrder:String,
        val addTime:String,
        val updateTime:String,
        val deleted:String
)