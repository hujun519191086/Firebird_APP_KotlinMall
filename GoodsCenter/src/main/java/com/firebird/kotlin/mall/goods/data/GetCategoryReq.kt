package com.firebird.kotlin.mall.goods.data

/*
  获取分类列表请求，parentId为0是一级分类
 */
data class GetCategoryReq (val id: Int)
