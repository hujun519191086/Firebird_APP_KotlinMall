package com.firebird.kotlin.mall.goods.di.module

import com.kotlin.goods.service.impl.CategoryServiceImpl
import com.firebird.kotlin.mall.goods.service.CategoryService
import dagger.Module
import dagger.Provides

/*
  商品分类Module
 */
@Module
class CategoryModule {

    @Provides
    fun provideCategoryService(categoryService: CategoryServiceImpl): CategoryService {
        return categoryService
    }
}
