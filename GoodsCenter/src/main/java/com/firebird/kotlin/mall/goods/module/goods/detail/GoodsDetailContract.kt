package com.firebird.kotlin.mall.goods.module.goods.detail

import com.firebird.kotlin.mall.goods.data.GoodDetails
import com.firebird.kotlin.mall.goods.data.Goods
import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface GoodsDetailContract {
    interface View : IBaseView {
        //获取商品详情
        fun onGetGoodsDetailResult(result: GoodDetails)
        fun onGetGoodsDetailError(message:String?)
        //加入购物车
        fun onAddCartResult(result: Int)
    }

    interface Presenter : IBasePresenter<View>
}