package com.firebird.kotlin.mall.goods.module.adapter


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebird.kotlin.mall.goods.R
import com.firebird.kotlin.mall.goods.data.Good
import com.firebird.kotlin.mall.goods.data.Goods
import com.firebird.kotlin.mall.library.ext.loadUrl
import com.firebird.kotlin.mall.library.mvp.adapter.BaseRecyclerViewAdapter
import com.firebird.kotlin.mall.library.util.YuanFenConverter
import kotlinx.android.synthetic.main.layout_goods_item.view.*

/*
  商品数据适配器
 */
class GoodsAdapter(context: Context) : BaseRecyclerViewAdapter<Good, GoodsAdapter.ViewHolder>(context) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(mContext)
                .inflate(R.layout.layout_goods_item,
                        parent,
                        false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        val model = dataList[position]
        //商品图标
        holder.itemView.mGoodsIconIv.loadUrl(model.picUrl)
        //商品描述
        holder.itemView.mGoodsDescTv.text = model.name
        //商品价格
        if (model.retailPrice.toString().isNullOrEmpty().not()){
            holder.itemView.mGoodsPriceTv.text =  "原价${model.counterPrice} 折扣价${model.retailPrice}"
        }else {
            holder.itemView.mGoodsPriceTv.text =  "原价${model.counterPrice} 折扣价${model.counterPrice}"
        }
       // holder.itemView.mGoodsPriceTv.text = YuanFenConverter.changeF2YWithUnit(model.counterPrice)
        //商品销量及库存
        holder.itemView.mGoodsSalesStockTv.text = model.brief
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
