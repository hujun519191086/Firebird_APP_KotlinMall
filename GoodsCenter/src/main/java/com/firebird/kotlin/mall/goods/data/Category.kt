package com.firebird.kotlin.mall.goods.data

/*
  商品分类
 */

data class Category(
       // val errno: Int,
       // val errmsg:String,
        val currentCategory: CurrentCategory,
        val brotherCategory: MutableList<BrotherCategory>,
        var isSelected: Boolean = false//是否被选中
)
data class CurrentCategory(
    val id:Int,val name:String,val keywords:String,val desc:String,val pid:Int,val iconUrl:String,val picUrl:String,
            val level:String,val sortOrder:Int,val deleted:Boolean=false
)
data class BrotherCategory(
    val id:Int,val name:String,val keywords:String,val desc:String,val pid:Int,
    val iconUrl:String,val picUrl:String,//分类图标
    val level:String,val sortOrder:Int,val deleted:Boolean=false,
    var isSelected: Boolean = false//是否被选中

)

