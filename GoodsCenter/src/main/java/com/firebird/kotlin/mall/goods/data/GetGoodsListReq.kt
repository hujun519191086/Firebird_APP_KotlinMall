package com.firebird.kotlin.mall.goods.data

/*
  按分类搜索商品
 */
data class GetGoodsListReq(val categoryId: Int,val pageNo: Int)
