package com.firebird.kotlin.mall.goods.injection.component

import com.firebird.kotlin.mall.goods.di.module.CartModule
import com.firebird.kotlin.mall.goods.di.module.GoodsModule
import com.firebird.kotlin.mall.goods.module.goods.list.GoodsListActivity
import com.firebird.kotlin.mall.library.di.component.ActivityComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import dagger.Component

/*
  商品Component
 */
@ComponentScope
@Component(dependencies = arrayOf(ActivityComponent::class), modules = arrayOf(GoodsModule::class, CartModule::class))
interface GoodsActivityComponent {
    fun inject(activity: GoodsListActivity)
}
