package com.firebird.kotlin.mall.goods.data

/*
  添加商品到购物车请求
 */
data class AddCartReq(
       // val id: Int,//购物车单项商品ID
        val userId:Int,
        val goodsId: Int,//具体商品ID
        val goodsSn: String,
        val goodsName: String,
        val productId: Long,//商品价格
        var price: Float,//商品价格
        val number: Int,//商品数量
        //val specifications:String,
        val checked:Boolean,
        val picUrl:String//商品图片
        //val addTime: String,
        //val updateTime: String,val deleted: Boolean
)
