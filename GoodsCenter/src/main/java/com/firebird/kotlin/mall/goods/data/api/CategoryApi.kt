package com.firebird.kotlin.mall.goods.data.api

import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.goods.data.Category
import com.firebird.kotlin.mall.goods.data.GetCategoryReq
import com.firebird.kotlin.mall.library.data.BaseResp
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable

/*
  商品分类接口
 */
interface CategoryApi {
    /*
      获取商品分类列表
     */
    @GET("wx/goods/category")
   // fun getCategory(@Body req: GetCategoryReq): Observable<BaseResp<MutableList<Category<BrotherCategory>>>>
    fun getCategory(@Query("id") id: Int): Observable<BaseResp<Category>>
}
