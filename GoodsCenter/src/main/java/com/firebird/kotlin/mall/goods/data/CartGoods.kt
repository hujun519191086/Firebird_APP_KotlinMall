package com.firebird.kotlin.mall.goods.data

/*
  购物车商品数据类
 */
data class CartGoods(
        val id: Int,//购物车单项商品ID
        val userId:Int,
        val goodsId: Int,//具体商品ID
        val goodsSn: String,//商品描述
        val goodsName: String,//商品图片
        val productId: Long,//商品价格
        var price: Int,//商品数量
        val number: String,//商品SKU
        val specifications:String,
        val checked:Boolean,
        val picUrl:String,
        val addTime: String,
        val updateTime: String,val deleted: Boolean,
        var isSelected: Boolean//是否选中
)
data class CartInfo(
        val id: Int,//购物车单项商品ID
        val userId:Int,
        val goodsId: Int,//具体商品ID
        val goodsSn: String,//商品描述
        val goodsName: String,//商品图片
        val productId: Long,//商品价格
        var price: Int,//商品数量
        val number: String,//商品SKU
        val specifications:String,
        val checked:Boolean,
        val picUrl:String,
        val addTime: String,
        val updateTime: String,val deleted: Boolean
)

