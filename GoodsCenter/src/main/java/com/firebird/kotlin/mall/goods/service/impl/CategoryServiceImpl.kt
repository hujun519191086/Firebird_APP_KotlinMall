package com.kotlin.goods.service.impl

import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.goods.data.Category
import com.firebird.kotlin.mall.goods.data.repository.CategoryRepository
import com.firebird.kotlin.mall.goods.service.CategoryService
import com.firebird.kotlin.mall.library.ext.convert
import rx.Observable
import javax.inject.Inject

/*
  商品分类 业务层 实现类
 */
class CategoryServiceImpl @Inject constructor() : CategoryService {
    @Inject
    lateinit var repository: CategoryRepository

    /*
      获取商品分类列表
     */
    override fun getCategory(id: Int): Observable<Category> {
        return repository.getCategory(id).convert()
    }
}
