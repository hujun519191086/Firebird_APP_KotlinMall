package com.firebird.kotlin.mall.goods.service.impl

import com.firebird.kotlin.mall.goods.data.AddCartReq
import com.firebird.kotlin.mall.goods.data.CartGoods
import com.firebird.kotlin.mall.goods.data.CartInfo
import com.firebird.kotlin.mall.goods.data.DeleteCartReq
import com.firebird.kotlin.mall.goods.data.repository.CartRepository
import com.firebird.kotlin.mall.goods.service.CartService
import com.firebird.kotlin.mall.library.data.BaseResp
import com.firebird.kotlin.mall.library.ext.convert
import com.firebird.kotlin.mall.library.ext.convertBoolean
import rx.Observable
import javax.inject.Inject

/*
  购物车 业务层实现类
 */
class CartServiceImpl @Inject constructor() : CartService {
    @Inject
    lateinit var repository: CartRepository

    /*
      加入购物车
     */
    override fun addCart(userId: Int, req: AddCartReq): Observable<Int> {
        return repository.addCart(userId,req)
    }

    /*
      获取购物车列表
     */
    override fun getCartList(userId: Int): Observable<MutableList<CartGoods>?> {
        return repository.getCartList(userId).convert()
    }

    /*
      删除购物车商品
     */
    override fun deleteCartList(userId: Int,deleteCartReq: DeleteCartReq): Observable<CartInfo> {
        return repository.deleteCartList(userId,deleteCartReq).convert()
    }

    /*
      提交购物车商品
     */
    override fun submitCart(list: MutableList<CartGoods>, totalPrice: Long): Observable<Int> {
        return repository.submitCart(list, totalPrice).convert()
    }

    /*
     更新购物车的数据
    */
    override fun updateCartGoods(list: MutableList<CartGoods>): Observable<Int> {
        return repository.updateCartGoods(list).convert()
    }
}
