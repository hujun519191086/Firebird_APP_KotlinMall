package com.firebird.kotlin.mall.goods.di.module

import com.firebird.kotlin.mall.goods.service.CartService
import com.firebird.kotlin.mall.goods.service.impl.CartServiceImpl
import dagger.Module
import dagger.Provides

/*
  购物车Module
 */
@Module
class CartModule {

    @Provides
    fun provideCartservice(cartService: CartServiceImpl): CartService {
        return cartService
    }
}
