package com.firebird.kotlin.mall.goods.module.category

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kennyc.view.MultiStateView
import com.firebird.kotlin.mall.goods.injection.component.DaggerCategoryComponent
import com.firebird.kotlin.mall.goods.R
import com.firebird.kotlin.mall.goods.core.GoodsConstant
import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.goods.data.Category
import com.firebird.kotlin.mall.goods.di.module.CategoryModule
import com.firebird.kotlin.mall.goods.module.adapter.SecondCategoryAdapter
import com.firebird.kotlin.mall.goods.module.adapter.TopCategoryAdapter
import com.firebird.kotlin.mall.goods.module.goods.list.GoodsListActivity
import com.firebird.kotlin.mall.library.ext.setVisible
import com.firebird.kotlin.mall.library.ext.startLoading
import com.firebird.kotlin.mall.library.mvp.adapter.BaseRecyclerViewAdapter
import com.firebird.kotlin.mall.library.mvp.view.fragment.BaseMvpFragment
import kotlinx.android.synthetic.main.fragment_category.*
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.startActivity

/*
    商品分类 Fragment
 */
class CategoryFragment : BaseMvpFragment<CategoryPresenter>(), CategoryContract.View {

    //一级分类Adapter
    lateinit var topAdapter: TopCategoryAdapter
    //二级分类Adapter
    lateinit var secondAdapter: SecondCategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        mPresenter.onAttach(this)
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        loadData()
    }

    override fun performInject() {
        DaggerCategoryComponent.builder().fragmentComponent(mFragmentComponent)
                .categoryModule(CategoryModule()).build().inject(this)
    }

    /*
      初始化视图
     */
    private fun initView() {
        mTopCategoryRv.layoutManager = LinearLayoutManager(context)
        topAdapter = TopCategoryAdapter(context!!)
        mTopCategoryRv.adapter = topAdapter
        //单项点击事件
        topAdapter.setOnItemClickListener(object : BaseRecyclerViewAdapter.OnItemClickListener<BrotherCategory> {
            override fun onItemClick(item: BrotherCategory, position: Int) {
                for (category in topAdapter.dataList) {
                    category.isSelected = item.id == category.id
                }
                topAdapter.notifyDataSetChanged()

                loadData(item.id)
            }
        })

        mSecondCategoryRv.layoutManager = GridLayoutManager(context, 3)
        secondAdapter = SecondCategoryAdapter(context!!)
        mSecondCategoryRv.adapter = secondAdapter
        secondAdapter.setOnItemClickListener(object : BaseRecyclerViewAdapter.OnItemClickListener<BrotherCategory> {
            override fun onItemClick(item: BrotherCategory, position: Int) {
                startActivity<GoodsListActivity>(GoodsConstant.KEY_CATEGORY_ID to item.id)
            }
        })
    }

    /*
      加载数据
     */
    private fun loadData(id: Int = 0) {
        if (id != 0) {
            mMultiStateView.startLoading()
        }
        mPresenter.getCategory(id)
    }

    /*
      获取商品分类 回调
     */
    override fun onGetCategoryResult(result: Category) {
        if (result != null && result.brotherCategory.size > 0) {
            if (result.brotherCategory[0].pid == 0) {
                result.brotherCategory[0].isSelected = true
                var y:Int?=0
                val categoryList=result.brotherCategory
                for (i in categoryList.indices){
                    val category=categoryList[i-y!!]
                    if(category.name=="root") {
                        categoryList.removeAt(i - y)
                        y++
                    }
                }
                //val category=result.brotherCategory.remove("v1")
                //topAdapter.setData(result.brotherCategory)
                topAdapter.setData(categoryList)
                //mPresenter.getCategory(result.brotherCategory[0].id)
                mPresenter.getCategory(categoryList[0].id)
            } else {
                secondAdapter.setData(result.brotherCategory)
                mTopCategoryIv.setVisible(true)
                mCategoryTitleTv.setVisible(true)
                mMultiStateView.viewState = MultiStateView.VIEW_STATE_CONTENT
            }
        } else {
            //没有数据
            mTopCategoryIv.setVisible(false)
            mCategoryTitleTv.setVisible(false)
            mMultiStateView.viewState = MultiStateView.VIEW_STATE_EMPTY
        }
    }
}
