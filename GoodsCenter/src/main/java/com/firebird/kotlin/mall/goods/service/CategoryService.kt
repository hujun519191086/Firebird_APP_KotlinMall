package com.firebird.kotlin.mall.goods.service

import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.goods.data.Category
import rx.Observable

/*
  商品分类 业务层 接口
 */
interface CategoryService {

    /*
      获取分类
     */
    fun getCategory(id:Int): Observable<Category>
}
