package com.firebird.kotlin.mall.goods.module.category

import com.firebird.kotlin.mall.goods.data.BrotherCategory
import com.firebird.kotlin.mall.goods.data.Category
import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface CategoryContract {
    interface View : IBaseView {
        //获取商品分类列表
        fun onGetCategoryResult(result: Category)
    }

    interface Presenter : IBasePresenter<View>
}