package com.firebird.kotlin.mall.goods.injection.component

import com.firebird.kotlin.mall.goods.di.module.CategoryModule
import com.firebird.kotlin.mall.goods.module.category.CategoryFragment
import com.firebird.kotlin.mall.library.di.component.FragmentComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import dagger.Component

/*
  商品分类Component
 */
@ComponentScope
@Component(dependencies = arrayOf(FragmentComponent::class), modules = arrayOf(CategoryModule::class))
interface CategoryComponent {
    fun inject(fragment: CategoryFragment)
}
