package com.firebird.kotlin.mall.goods.module.goods.list

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import cn.bingoogolapple.refreshlayout.BGANormalRefreshViewHolder
import cn.bingoogolapple.refreshlayout.BGARefreshLayout
import com.kennyc.view.MultiStateView
import com.firebird.kotlin.mall.goods.R
import com.firebird.kotlin.mall.goods.core.GoodsConstant
import com.firebird.kotlin.mall.goods.data.Good
import com.firebird.kotlin.mall.goods.data.Goods
import com.firebird.kotlin.mall.goods.di.module.GoodsModule
import com.firebird.kotlin.mall.goods.injection.component.DaggerGoodsActivityComponent
import com.firebird.kotlin.mall.goods.module.adapter.GoodsAdapter
import com.firebird.kotlin.mall.goods.module.goods.detail.GoodsDetailActivity
import com.firebird.kotlin.mall.library.ext.startLoading
import com.firebird.kotlin.mall.library.mvp.adapter.BaseRecyclerViewAdapter
import com.firebird.kotlin.mall.library.mvp.view.activity.BaseMvpActivity
import kotlinx.android.synthetic.main.activity_goods.*
import org.jetbrains.anko.startActivity

/*
  商品列表Activity
 */
class GoodsListActivity : BaseMvpActivity<GoodsListPresenter>(), GoodListContract.View, BGARefreshLayout.BGARefreshLayoutDelegate {

    private lateinit var mGoodsAdapter: GoodsAdapter
    private var mCurrentPage: Int = 1
    private var mMaxPage: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPresenter.onAttach(this)
        setContentView(R.layout.activity_goods)

        initView()
        initRefreshLayout()
        loadData()
    }

    override fun performInject() {
        DaggerGoodsActivityComponent.builder().activityComponent(mActivityComponent)
                .goodsModule(GoodsModule()).build().inject(this)
    }

    /*
        初始化视图
     */
    private fun initView() {
        mGoodsRv.layoutManager = GridLayoutManager(this, 3)
        mGoodsAdapter = GoodsAdapter(this)
        mGoodsRv.adapter = mGoodsAdapter

        mGoodsAdapter.setOnItemClickListener(object : BaseRecyclerViewAdapter.OnItemClickListener<Good> {
            override fun onItemClick(item: Good, position: Int) {
                startActivity<GoodsDetailActivity>(GoodsConstant.KEY_GOODS_ID to item.id)
            }
        })

    }

    /*
        初始化刷新视图
     */
    private fun initRefreshLayout() {
        mRefreshLayout.setDelegate(this)
        val viewHolder = BGANormalRefreshViewHolder(this, true)
        viewHolder.setLoadMoreBackgroundColorRes(R.color.common_bg)
        viewHolder.setRefreshViewBackgroundColorRes(R.color.common_bg)
        mRefreshLayout.setRefreshViewHolder(viewHolder)
    }

    /*
        加载数据
     */
    private fun loadData() {
        if (intent.getIntExtra(GoodsConstant.KEY_SEARCH_GOODS_TYPE, 0) != 0) {
            mMultiStateView.startLoading()
            mPresenter.getGoodsListByKeyword(intent.getStringExtra(GoodsConstant.KEY_GOODS_KEYWORD), mCurrentPage)
        } else {
            mMultiStateView.startLoading()
            mPresenter.getGoodsList(intent.getIntExtra(GoodsConstant.KEY_CATEGORY_ID, 1), mCurrentPage)
        }
    }

    /*
      获取列表后回调
     */
    override fun onGetGoodsListResult(result: Goods) {
        mRefreshLayout.endLoadingMore()
        mRefreshLayout.endRefreshing()
        if (result != null && result.list.size > 0) {
            mMaxPage = result.pages
            if (mCurrentPage == 1) {
                mGoodsAdapter.setData(result.list)
            } else {
                mGoodsAdapter.dataList.addAll(result.list)
                mGoodsAdapter.notifyDataSetChanged()
            }
            mMultiStateView.viewState = MultiStateView.VIEW_STATE_CONTENT
        } else {
            mMultiStateView.viewState = MultiStateView.VIEW_STATE_EMPTY
        }
    }

    /*
        上拉加载更多
     */
    override fun onBGARefreshLayoutBeginLoadingMore(refreshLayout: BGARefreshLayout?): Boolean {
        return if (mCurrentPage < mMaxPage) {
            mCurrentPage++
            loadData()
            true
        } else {
            false
        }
    }

    /*
        下拉加载第一页
     */
    override fun onBGARefreshLayoutBeginRefreshing(refreshLayout: BGARefreshLayout?) {
        mCurrentPage = 1
        loadData()
    }
}
