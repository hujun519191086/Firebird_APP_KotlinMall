package com.firebird.kotlin.mall.goods.data.api

import com.firebird.kotlin.mall.goods.data.*
import com.firebird.kotlin.mall.library.data.BaseResp
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable

/*
  商品接口
 */
interface GoodsApi {
    /*
      获取商品列表
     */
    @GET("wx/goods/list")
   // fun getGoodsList(@Body req: GetGoodsListReq): Observable<BaseResp<MutableList<Goods>?>>
    fun getGoodsList(
            @Query("categoryId") categoryId: Int,
            @Query("page") page: Int
            //@Query("limit") limit: Int
            //@Query("isHot") isHot: Boolean,
    ): Observable<BaseResp<Goods>>

    /*
      获取商品列表
     */
    @GET("wx/goods/list")
    //fun getGoodsListByKeyword(@Body req: GetGoodsListByKeywordReq): Observable<BaseResp<MutableList<Goods>?>>
    fun getGoodsListByKeyword( @Query("keyword") keyword: String,
                               @Query("page") page: Int): Observable<BaseResp<Goods>>

    /*
      获取商品详情
     */
    @GET("wx/goods/detail")
    fun getGoodsDetail(@Query("id") id:Int): Observable<GoodDetails>
}
