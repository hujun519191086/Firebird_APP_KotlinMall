package com.firebird.kotlin.mall.goods.di.component

import com.firebird.kotlin.mall.goods.di.module.CartModule
import com.firebird.kotlin.mall.goods.module.cart.CartFragment
import com.firebird.kotlin.mall.library.di.component.FragmentComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import dagger.Component

/*
  购物车Component
 */
@ComponentScope
@Component(dependencies = arrayOf(FragmentComponent::class), modules = arrayOf(CartModule::class))
interface CartComponent {
    fun inject(fragment: CartFragment)
}
