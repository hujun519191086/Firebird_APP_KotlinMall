package com.firebird.kotlin.mall.goods.service

import com.firebird.kotlin.mall.goods.data.*
import com.firebird.kotlin.mall.goods.data.api.CartApi
import com.firebird.kotlin.mall.library.data.BaseResp
import com.firebird.kotlin.mall.library.data.net.RetrofitFactory
import rx.Observable

/*
  购物车 业务接口
 */
interface CartService {
    /*
      添加商品到购物车
     */
    fun addCart(userId: Int, cart: AddCartReq): Observable<Int>

    /*
      获取购物车列表
     */
    fun getCartList(userId: Int): Observable<MutableList<CartGoods>?>

    /*
      删除购物车商品
     */
    fun deleteCartList(userId: Int,deleteCartReq: DeleteCartReq): Observable<CartInfo>

    /*
      购物车结算
    */
    fun submitCart(list: MutableList<CartGoods>, totalPrice: Long): Observable<Int>

    /*
     更新购物车的数据
    */
    fun updateCartGoods(list: MutableList<CartGoods>): Observable<Int>
}
