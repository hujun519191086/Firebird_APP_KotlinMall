package com.firebird.kotlin.mall.order.module.order.list

import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView
import com.firebird.kotlin.mall.order.data.Order

interface OrderListContract {
    interface View : IBaseView {
        //获取订单列表回调
        fun onGetOrderListResult(result:MutableList<Order>?)
        //确认订单回调
        fun onConfirmOrderResult(result:Boolean)
        //取消订单回调
        fun onCancelOrderResult(result:Boolean)
    }

    interface Presenter : IBasePresenter<View>
}