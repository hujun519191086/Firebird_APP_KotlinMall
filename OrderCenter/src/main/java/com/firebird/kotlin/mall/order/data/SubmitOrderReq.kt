package com.kotlin.order.data.protocol

import com.firebird.kotlin.mall.order.data.Order

/*
  提交订单
 */
data class SubmitOrderReq(val order: Order)
