package com.firebird.kotlin.mall.order.di.component

import com.firebird.kotlin.mall.library.di.component.ActivityComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import com.firebird.kotlin.mall.order.di.module.AddressModule
import com.firebird.kotlin.mall.order.module.address.edit.AddressEditActivity
import com.firebird.kotlin.mall.order.module.address.list.AddressListActivity
import dagger.Component

/*
  收货人信息Component
 */
@ComponentScope
@Component(dependencies = arrayOf(ActivityComponent::class),modules = arrayOf(AddressModule::class))
interface AddressComponent {
    fun inject(activity: AddressEditActivity)
    fun inject(activity: AddressListActivity)
}
