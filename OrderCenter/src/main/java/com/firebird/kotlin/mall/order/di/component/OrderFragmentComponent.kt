package com.firebird.kotlin.mall.order.di.component

import com.firebird.kotlin.mall.library.di.component.FragmentComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import com.firebird.kotlin.mall.order.di.module.OrderModule
import com.firebird.kotlin.mall.order.module.order.list.OrderListFragment
import dagger.Component

/*
  订单Component
 */
@ComponentScope
@Component(dependencies = arrayOf(FragmentComponent::class), modules = arrayOf(OrderModule::class))
interface OrderFragmentComponent {
    fun inject(fragment: OrderListFragment)
}
