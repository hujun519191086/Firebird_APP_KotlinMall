package com.firebird.kotlin.mall.order.data

/*
  取消订单
 */
data class CancelOrderReq(val orderId:Int)
