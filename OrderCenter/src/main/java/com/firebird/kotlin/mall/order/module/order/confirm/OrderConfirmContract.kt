package com.firebird.kotlin.mall.order.module.order.confirm

import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView
import com.firebird.kotlin.mall.order.data.Order

interface OrderConfirmContract {
    interface View : IBaseView {
        //获取订单回调
        fun onGetOrderByIdResult(result: Order)
        //提交订单回调
        fun onSubmitOrderResult(result:Boolean)
    }

    interface Presenter : IBasePresenter<View>
}