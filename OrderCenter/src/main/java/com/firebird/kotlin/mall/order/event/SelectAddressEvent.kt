package com.firebird.kotlin.mall.order.event

import com.firebird.kotlin.mall.order.data.Address

/*
  选择收货人信息事件
 */
class SelectAddressEvent(val address: Address)
