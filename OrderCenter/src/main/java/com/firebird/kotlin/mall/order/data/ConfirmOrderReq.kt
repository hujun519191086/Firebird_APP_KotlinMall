package com.firebird.kotlin.mall.order.data

/*
  确认订单
 */
data class ConfirmOrderReq(val orderId:Int)
