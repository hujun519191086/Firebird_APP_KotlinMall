package com.firebird.kotlin.mall.order.module.order.detail

import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView
import com.firebird.kotlin.mall.order.data.Order

interface OrderDetailContract {
    interface View : IBaseView {
        //获取订单回调
        fun onGetOrderByIdResult(result: Order)
    }

    interface Presenter : IBasePresenter<View>
}