package com.firebird.kotlin.mall.order.di.component

import com.firebird.kotlin.mall.library.di.component.ActivityComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import com.firebird.kotlin.mall.order.di.module.OrderModule
import com.firebird.kotlin.mall.order.module.order.confirm.OrderConfirmActivity
import com.firebird.kotlin.mall.order.module.order.detail.OrderDetailActivity
import dagger.Component

/*
  订单Component
 */
@ComponentScope
@Component(dependencies = arrayOf(ActivityComponent::class), modules = arrayOf(OrderModule::class))
interface OrderActivityComponent {
    fun inject(activity: OrderConfirmActivity)
    fun inject(activity: OrderDetailActivity)
}
