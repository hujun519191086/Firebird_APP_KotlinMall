package com.firebird.kotlin.mall.user.service

import com.kotlin.user.data.protocol.LoginResultResponse
import com.kotlin.user.data.protocol.RegisterResultResponse
import rx.Observable

/*
  用户模块 业务接口
 */
interface UserService {

    //用户注册
    fun register(username: String,password:String,mobile: String,code:String):Observable<RegisterResultResponse>

    //用户登录
    fun login(username: String,password:String,pushId:String):Observable<LoginResultResponse>

    //忘记密码
    fun forgetPwd(mobile:String,verifyCode:String):Observable<Boolean>

    //重置密码
    fun resetPwd(password: String, mobile: String,code: String):Observable<Boolean>

    //编辑用户资料
    fun editUser(userIcon:String,userName:String,userGender:String,userSign:String):Observable<LoginResultResponse>
}
