package com.firebird.kotlin.mall.user.module.resetpwd

import android.os.Bundle
import com.kotlin.user.injection.component.DaggerUserComponent
import com.kotlin.user.injection.module.UserModule
import com.firebird.kotlin.mall.library.ext.enable
import com.firebird.kotlin.mall.library.ext.onClick
import com.firebird.kotlin.mall.library.mvp.view.activity.BaseMvpActivity
import com.firebird.kotlin.mall.user.R
import com.firebird.kotlin.mall.user.module.login.LoginActivity
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_reset_pwd.*
import kotlinx.android.synthetic.main.activity_reset_pwd.mPwdConfirmEt
import kotlinx.android.synthetic.main.activity_reset_pwd.mPwdEt
import org.jetbrains.anko.clearTop
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.singleTop
import org.jetbrains.anko.toast

/*
  重置密码界面
 */
class ResetPwdActivity : BaseMvpActivity<ResetPwdPresenter>(), ResetPwdContract.View {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.onAttach(this)
        setContentView(R.layout.activity_reset_pwd)

        initView()
    }

    override fun performInject() {
        DaggerUserComponent.builder().activityComponent(mActivityComponent)
                .userModule(UserModule()).build().inject(this)
    }

    /*
      初始化视图
     */
    private fun initView() {
        mConfirmBtn.onClick {
            if (mPwdEt.text.toString() != mPwdConfirmEt.text.toString()) {
                toast("密码不一致")
                return@onClick
            }
            mPresenter.resetPwd(intent.getStringExtra("mobile"), mPwdEt.text.toString(),mVerifyCodeEt.text.toString())
        }
    }

    /*
      重置密码回调
     */
    override fun onResetPwdResult(result: String) {
        toast(result)
        startActivity(intentFor<LoginActivity>().singleTop().clearTop())
    }
}
