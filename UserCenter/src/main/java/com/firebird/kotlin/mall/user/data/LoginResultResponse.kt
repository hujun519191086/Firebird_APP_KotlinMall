package com.kotlin.user.data.protocol

/**
 * 登录返回result
 * token	用户登录生成的token
 * uid	用户Id
 */
data class LoginResultResponse(val errno: Int, val errmsg: String,val data:LoginResultData,val token: String)
data class LoginResultData(val userInfo: UserInfo)
data class UserInfo(val nickName:String,val avatarUrl:String,val gender:Int,val id:Int)

