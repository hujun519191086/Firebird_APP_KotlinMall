package com.firebird.kotlin.mall.user.service.impl

import com.kotlin.user.data.protocol.LoginResultResponse
import com.firebird.kotlin.mall.library.ext.convert
import com.firebird.kotlin.mall.library.ext.convertBoolean
import com.firebird.kotlin.mall.user.data.repository.UserRepository
import com.firebird.kotlin.mall.user.service.UserService
import com.kotlin.user.data.protocol.RegisterResultResponse
import rx.Observable
import javax.inject.Inject

/*
  用户模块业务实现类
 */
class UserServiceImpl @Inject constructor() : UserService {
    @Inject
    lateinit var repository: UserRepository

    /*
      注册
     */
    override fun register(username: String,password:String,mobile: String,code:String): Observable<RegisterResultResponse> {
        return repository.register(username, password, mobile, code)
    }

    /*
      登录
     */
    override fun login(username: String, password: String,pushId:String): Observable<LoginResultResponse> {
        return repository.login(username, password,pushId)
    }

    /*
      忘记密码
     */
    override fun forgetPwd(mobile: String, verifyCode: String): Observable<Boolean> {
        return repository.forgetPwd(mobile, verifyCode).convertBoolean()
    }

    /*
      重置密码
     */
    override fun resetPwd(password: String, mobile: String,code: String): Observable<Boolean> {
        return repository.resetPwd(password, mobile, code).convertBoolean()
    }

    /*
      修改用户资料
     */
    override fun editUser(userIcon: String, userName: String, userGender: String, userSign: String): Observable<LoginResultResponse> {
        return repository.editUser(userIcon, userName, userGender, userSign).convert()
    }

}
