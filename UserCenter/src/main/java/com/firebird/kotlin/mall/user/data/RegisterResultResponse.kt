package com.kotlin.user.data.protocol

/**
 * 登录返回result
 * token	用户登录生成的token
 * uid	用户Id
 */
data class RegisterResultResponse(val errno: Int, val errmsg:String,val data: RegisterResultData)
data class RegisterResultData(val token: String, val tokenExpire:Int,val userInfo: NewUserInfo)
data class NewUserInfo(val nickName:String,val avatarUrl:String)

