package com.firebird.kotlin.mall.user.data.api

import com.kotlin.user.data.protocol.*
import com.firebird.kotlin.mall.library.data.BaseResp
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import rx.Observable

/*
  用户相关 接口
 */
interface UserApi {

    /*
      用户注册
     */
    @POST("wx/auth/register")
    fun register(
            @Body req:RegisterReq
    ): Observable<RegisterResultResponse>

    /*
      用户登录
     */
    @POST("wx/auth/login")
    fun login(@Body req: LoginReq): Observable<LoginResultResponse>

    /**
     * 请求验证码
     *
     * TODO
     * 这里需要一定机制防止短信验证码被滥用
     *
     * @param body 手机号码 { mobile: xxx, type: xxx }
     * @return
     */
    @POST("wx/auth/captcha")
    fun Captcha(@Body req: CaptchaReq): Observable<BaseResp<Boolean>>

    /*
      忘记密码
     */
    @POST("userCenter/forgetPwd")
    fun forgetPwd(@Body req: ForgetPwdReq): Observable<BaseResp<String>>

    /**
     * 账号密码重置
     *
     * @param body    请求内容
     *                {
     *                password: xxx,
     *                mobile: xxx
     *                code: xxx
     *                }
     *                其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果
     * 成功则 { errno: 0, errmsg: '成功' }
     * 失败则 { errno: XXX, errmsg: XXX }
     */
    @POST("wx/auth/reset")
    fun resetPwd(@Body req: ResetPwdReq): Observable<BaseResp<String>>

    /*
      编辑用户资料
     */
    @POST("userCenter/editUser")
    fun editUser(@Body req: EditUserReq): Observable<BaseResp<LoginResultResponse>>
}
