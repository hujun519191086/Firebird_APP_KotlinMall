package com.kotlin.user.data.protocol

/*
    登录请求体
 */
data class CaptchaReq(val mobile:String, val type:String)
