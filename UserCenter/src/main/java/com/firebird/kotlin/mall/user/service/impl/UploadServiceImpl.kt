package com.firebird.kotlin.mall.user.service.impl

import com.firebird.kotlin.mall.library.ext.convert
import com.firebird.kotlin.mall.user.data.repository.UploadRepository
import com.firebird.kotlin.mall.user.service.UploadService
import rx.Observable
import javax.inject.Inject

/*
  上传业务实现类
 */
class UploadServiceImpl @Inject constructor(): UploadService {

    @Inject
    lateinit var repository: UploadRepository

    override fun getUploadToken(): Observable<String> {
       return repository.getUploadToken().convert()
    }
}
