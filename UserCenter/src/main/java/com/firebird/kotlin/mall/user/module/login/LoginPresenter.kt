package com.firebird.kotlin.mall.user.module.login

import com.kotlin.user.data.protocol.LoginResultResponse
import com.firebird.kotlin.mall.library.ext.excute
import com.firebird.kotlin.mall.library.mvp.presenter.BasePresenter
import com.firebird.kotlin.mall.library.rx.BaseSubscriber
import com.firebird.kotlin.mall.user.service.UserService
import javax.inject.Inject

/*
  登录界面 Presenter
 */
class LoginPresenter @Inject constructor() : BasePresenter<LoginContract.View>(), LoginContract.Presenter {

    @Inject
    lateinit var userService: UserService

    /*
      登录
     */
    fun login(username: String, password: String, pushId: String) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()
        userService.login(username, password,pushId).excute(object : BaseSubscriber<LoginResultResponse>(getView()) {
            override fun onNext(t: LoginResultResponse) {
                    getView().onLoginResult(t)
            }

            override fun onError(e: Throwable?) {
                getView().onLoginError(e!!.message)
            }

        }, mLifecycleProvider)
    }
}
