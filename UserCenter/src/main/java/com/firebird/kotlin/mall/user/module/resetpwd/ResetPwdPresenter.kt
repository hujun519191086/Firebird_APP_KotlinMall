package com.firebird.kotlin.mall.user.module.resetpwd

import com.firebird.kotlin.mall.library.ext.excute
import com.firebird.kotlin.mall.library.mvp.presenter.BasePresenter
import com.firebird.kotlin.mall.library.rx.BaseSubscriber
import com.firebird.kotlin.mall.user.service.UserService
import javax.inject.Inject

/*
  重置密码Presenter
 */
class ResetPwdPresenter @Inject constructor() : BasePresenter<ResetPwdContract.View>(), ResetPwdContract.Presenter {

    @Inject
    lateinit var userService: UserService

    /*
      重置密码
     */
    fun resetPwd(password: String, mobile: String,code: String){
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()

        userService.resetPwd(password, mobile, code).excute(object : BaseSubscriber<Boolean>(getView()) {
            override fun onNext(t: Boolean) {
                if (t)
                    getView().onResetPwdResult("重置密码成功")
            }
        }, mLifecycleProvider)

    }

}
