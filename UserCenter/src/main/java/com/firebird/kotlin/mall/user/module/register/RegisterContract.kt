package com.firebird.kotlin.mall.user.module.register

import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView
import com.kotlin.user.data.protocol.RegisterResultResponse

interface RegisterContract {
    interface View : IBaseView {
        fun onRegisterResult(result:RegisterResultResponse)
        fun onRegisterError(message:String?)
    }

    interface Presenter : IBasePresenter<View> {
    }
}