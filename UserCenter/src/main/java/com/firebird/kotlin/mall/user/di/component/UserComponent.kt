package com.kotlin.user.injection.component

import com.kotlin.user.injection.module.UploadModule
import com.kotlin.user.injection.module.UserModule
import com.firebird.kotlin.mall.library.di.component.ActivityComponent
import com.firebird.kotlin.mall.library.di.scope.ComponentScope
import com.firebird.kotlin.mall.user.module.forgetpwd.ForgetPwdActivity
import com.firebird.kotlin.mall.user.module.login.LoginActivity
import com.firebird.kotlin.mall.user.module.register.RegisterActivity
import com.firebird.kotlin.mall.user.module.resetpwd.ResetPwdActivity
import com.firebird.kotlin.mall.user.module.user.UserInfoActivity
import dagger.Component

/*
  用户模块Component
 */
@ComponentScope
@Component(dependencies = arrayOf(ActivityComponent::class), modules = arrayOf(UserModule::class, UploadModule::class))
interface UserComponent {
    fun inject(activity: RegisterActivity)
    fun inject(activity: LoginActivity)
    fun inject(activity: ForgetPwdActivity)
    fun inject(activity: ResetPwdActivity)
    fun inject(activity: UserInfoActivity)
}
