package com.firebird.kotlin.mall.user.data.api

import com.firebird.kotlin.mall.library.data.BaseResp
import retrofit2.http.POST
import rx.Observable

/*
  上传相关 接口
 */
interface UploadApi {

    /*
      获取七牛云上传凭证
     */
    @POST("common/getUploadToken")
    fun getUploadToken(): Observable<BaseResp<String>>
}
