package com.kotlin.user.injection.module

import com.firebird.kotlin.mall.user.service.UserService
import com.firebird.kotlin.mall.user.service.impl.UserServiceImpl
import dagger.Module
import dagger.Provides

/*
  用户模块Module
 */
@Module
class UserModule {

    @Provides
    fun provideUserService(userService: UserServiceImpl): UserService {
        return userService
    }
}

