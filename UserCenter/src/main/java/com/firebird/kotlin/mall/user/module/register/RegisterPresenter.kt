package com.firebird.kotlin.mall.user.module.register

import com.firebird.kotlin.mall.library.ext.excute
import com.firebird.kotlin.mall.library.mvp.presenter.BasePresenter
import com.firebird.kotlin.mall.library.rx.BaseSubscriber
import com.firebird.kotlin.mall.user.service.UserService
import com.kotlin.user.data.protocol.RegisterResultResponse
import javax.inject.Inject

/*
  用户注册Presenter
 */
class RegisterPresenter @Inject constructor() : BasePresenter<RegisterContract.View>(), RegisterContract.Presenter {

    @Inject
    lateinit var userService: UserService

    fun register(username: String,password:String,mobile: String,code:String) {
        if (!checkNetWork()) {
            return
        }
        getView().showLoading()

        userService.register(username, password, mobile, code).excute(object : BaseSubscriber<RegisterResultResponse>(getView()) {
            override fun onNext(t: RegisterResultResponse) {
                    getView().onRegisterResult(t)
            }

            override fun onError(e: Throwable?) {
                getView().onRegisterError(e!!.message)
            }
        }, mLifecycleProvider)
    }
}
