package com.firebird.kotlin.mall.user.util

import com.kotlin.base.common.BaseConstant
import com.kotlin.user.data.protocol.LoginResultResponse
import com.firebird.kotlin.mall.library.util.AppPrefsUtils
import com.firebird.kotlin.mall.provider.common.ProviderConstant

/*
  本地存储用户相关信息
 */
object UserPrefsUtils {

    /*
      退出登录时，传入null,清空存储
     */
    fun putUserInfo(loginResultResponse: LoginResultResponse?) {
        AppPrefsUtils.instant.putString(BaseConstant.KEY_SP_TOKEN, "token" ?: "")
        AppPrefsUtils.instant.putString(ProviderConstant.KEY_SP_USER_Id, loginResultResponse?.data?.userInfo?.id.toString())
        AppPrefsUtils.instant.putString(ProviderConstant.KEY_SP_USER_AVATAR, loginResultResponse?.data?.userInfo?.avatarUrl ?: "")
        AppPrefsUtils.instant.putString(ProviderConstant.KEY_SP_USER_NICENAME, loginResultResponse?.data?.userInfo?.nickName ?: "")
        //AppPrefsUtils.instant.putString(ProviderConstant.KEY_SP_USER_MOBILE, loginResultResponse?.userMobile ?: "")
        AppPrefsUtils.instant.putString(ProviderConstant.KEY_SP_USER_GENDER, loginResultResponse?.data?.userInfo?.gender.toString())
    }
}
