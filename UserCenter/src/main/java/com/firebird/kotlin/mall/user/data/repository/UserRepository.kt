package com.firebird.kotlin.mall.user.data.repository

import com.kotlin.user.data.protocol.*
import com.firebird.kotlin.mall.library.data.BaseResp
import com.firebird.kotlin.mall.library.data.net.RetrofitFactory
import com.firebird.kotlin.mall.user.data.api.UserApi
import rx.Observable
import javax.inject.Inject

/*
  用户相关数据层
 */
class UserRepository @Inject constructor() {
    /*
      用户注册
     */
    fun register(username: String,password:String,mobile: String,code:String): Observable<RegisterResultResponse> {
        return RetrofitFactory.instance.create(UserApi::class.java).register(RegisterReq(username,password,mobile,code))
    }

    /*
      用户登录
     */
    fun login(username: String, password: String,pushId:String): Observable<LoginResultResponse> {
        return RetrofitFactory.instance.create(UserApi::class.java).login(LoginReq(username, password,pushId))
    }

    /*
      忘记密码
     */
    fun forgetPwd(mobile: String, verifyCode: String): Observable<BaseResp<String>> {
        return RetrofitFactory.instance.create(UserApi::class.java).forgetPwd(ForgetPwdReq(mobile, verifyCode))
    }

    /*
      重置密码
     */
    fun resetPwd(password: String, mobile: String,code: String): Observable<BaseResp<String>> {
        return RetrofitFactory.instance.create(UserApi::class.java).resetPwd(ResetPwdReq(password, mobile, code))
    }

    /*
      编辑用户资料
     */
    fun editUser(userIcon: String, userName: String, userGender: String, userSign: String): Observable<BaseResp<LoginResultResponse>> {
        return RetrofitFactory.instance.create(UserApi::class.java).editUser(EditUserReq(userIcon, userName, userGender, userSign))
    }
}
