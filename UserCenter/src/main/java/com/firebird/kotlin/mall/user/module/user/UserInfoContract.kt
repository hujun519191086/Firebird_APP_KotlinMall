package com.firebird.kotlin.mall.user.module.user

import com.kotlin.user.data.protocol.LoginResultResponse
import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface UserInfoContract {
    interface View : IBaseView {
        /*
          获取上传凭证回调
         */
        fun onGetUploadTokenResult(result:String)

        /*
          编辑用户资料回调
         */
        fun onEditUserResult(result: LoginResultResponse)
    }

    interface Presenter : IBasePresenter<View> {

    }
}