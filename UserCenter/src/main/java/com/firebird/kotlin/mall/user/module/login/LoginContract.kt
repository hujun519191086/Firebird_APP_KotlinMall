package com.firebird.kotlin.mall.user.module.login

import com.kotlin.user.data.protocol.LoginResultResponse
import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface LoginContract {
    interface View : IBaseView {
        fun onLoginResult(result: LoginResultResponse)
        fun onLoginError(message:String?)
    }

    interface Presenter : IBasePresenter<View> {

    }
}