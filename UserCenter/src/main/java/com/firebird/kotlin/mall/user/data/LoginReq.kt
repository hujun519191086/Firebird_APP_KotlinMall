package com.kotlin.user.data.protocol

/*
    登录请求体
 */
data class LoginReq(val username:String, val password:String,val pushId:String)
