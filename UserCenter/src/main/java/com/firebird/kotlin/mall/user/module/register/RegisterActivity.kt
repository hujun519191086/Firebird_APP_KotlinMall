package com.firebird.kotlin.mall.user.module.register

import android.os.Bundle
import android.text.TextUtils
import android.view.View
import cn.pedant.SweetAlert.SweetAlertDialog
import com.kotlin.user.injection.component.DaggerUserComponent
import com.kotlin.user.injection.module.UserModule
import com.firebird.kotlin.mall.library.ext.enable
import com.firebird.kotlin.mall.library.ext.onClick
import com.firebird.kotlin.mall.library.mvp.view.activity.BaseMvpActivity
import com.firebird.kotlin.mall.user.R
import com.firebird.kotlin.mall.user.util.UserPrefsUtils
import com.kotlin.user.data.protocol.RegisterResultResponse
import kotlinx.android.synthetic.main.activity_register.*
import org.jetbrains.anko.toast
import java.util.regex.Pattern

/*
  注册界面
 */
class RegisterActivity : BaseMvpActivity<RegisterPresenter>(), RegisterContract.View, View.OnClickListener {

    var dialog:SweetAlertDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPresenter.onAttach(this)
        setContentView(R.layout.activity_register)

        initView()
    }

    override fun performInject() {
        DaggerUserComponent.builder().activityComponent(mActivityComponent)
                .userModule(UserModule()).build().inject(this)
    }

    /*
      初始化视图
     */
    private fun initView() {
        mRegisterBtn.enable(mMobileEt, { isBtnEnable() })
        mRegisterBtn.enable(mVerifyCodeEt, { isBtnEnable() })
        mRegisterBtn.enable(mPwdEt, { isBtnEnable() })
        mRegisterBtn.enable(mPwdConfirmEt, { isBtnEnable() })

        mVerifyCodeBtn.onClick(this)
        mRegisterBtn.onClick(this)
    }

    /*
      注册回调
     */
    override fun onRegisterResult(result: RegisterResultResponse) {
        this.hideLoading()
        dialog?.changeAlertType(SweetAlertDialog.SUCCESS_TYPE)
        dialog?.titleText = result.errmsg
        if(result.errno==0){
            //UserPrefsUtils.putUserInfo(result)
            finish()
        }
    }

    override fun onRegisterError(message: String?) {
        this.hideLoading()
        dialog?.changeAlertType(SweetAlertDialog.ERROR_TYPE)
        dialog?.titleText = message
    }

    /*
        点击事件
     */
    override fun onClick(view: View) {
        when (view.id) {
            R.id.mVerifyCodeBtn -> {
                mVerifyCodeBtn.requestSendVerifyNumber()
                toast("发送验证成功")
            }

            R.id.mRegisterBtn -> {
                if (checkContent()) {
                    dialog = SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
                            .setTitleText("正在注册...")
                    dialog?.setCancelable(false)
                    dialog?.show()
                    //loginPresenter?.register(username.text.toString(), password.text.toString(), email.text.toString())
                    mPresenter.register(mMobileEt.text.toString(), mPwdEt.text.toString(), mMobileEt.text.toString(), mVerifyCodeEt.text.toString())
                }
            }
        }
    }
    /**
     * 手机号的正则校验
     */
    private fun regexPhone(phone: String): Boolean {
        var mainRegex = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,1,2,3,5-9])|(177))\\d{8}$"
        var p = Pattern.compile(mainRegex)
        val m = p.matcher(phone)
        return m.matches()
    }
    /**
     * 判断
     */
    private fun checkContent(): Boolean {
        mMobileEt.error = null
        mPwdEt.error = null
        mVerifyCodeEt.error = null

        var cancel = false
        var focusView: View? = null

        if (TextUtils.isEmpty(mMobileEt.text.toString())) {
            mMobileEt.error = "手机号不能为空"
            focusView = mMobileEt
            cancel = true
        }
        /*else if(regexPhone(mMobileEt.text.toString())){
            mMobileEt.error = "手机号格式不对"
            focusView = mMobileEt
            cancel = true
        }*/

        if (TextUtils.isEmpty(mPwdEt.text.toString())) {
            mPwdEt.error = "密码不能为空"
            focusView = mPwdEt
            cancel = true
        } else if (mPwdEt.text.length < 6) {
            mPwdEt.error = "密码长度不能小于6位"
            focusView = mPwdEt
            cancel = true
        }

        if (TextUtils.isEmpty(mPwdConfirmEt.text.toString())) {
            mPwdConfirmEt.error = "确认密码不能为空"
            focusView = mPwdConfirmEt
            cancel = true
        } else if (mPwdConfirmEt.text.length < 6) {
            mPwdConfirmEt.error = "确认密码长度不能小于6位"
            focusView = mPwdConfirmEt
            cancel = true
        }

        if (mPwdConfirmEt.text.toString()!=mPwdEt.text.toString()) {
            mPwdConfirmEt.error = "确认密码与原密码不符"
            focusView = mPwdConfirmEt
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            if (focusView != null) {
                focusView.requestFocus()
            }
        } else {
            return true
        }
        return false
    }


    /*
      判断按钮是否可用
     */
    private fun isBtnEnable(): Boolean {
        return mMobileEt.text.isNullOrEmpty().not() &&
                mVerifyCodeEt.text.isNullOrEmpty().not() &&
                mPwdEt.text.isNullOrEmpty().not() &&
                mPwdConfirmEt.text.isNullOrEmpty().not()
    }

}
