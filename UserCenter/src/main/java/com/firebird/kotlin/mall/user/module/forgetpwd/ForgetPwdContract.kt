package com.firebird.kotlin.mall.user.module.forgetpwd

import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface ForgetPwdContract {
    interface View : IBaseView {
        /*
          忘记密码回调
        */
        fun onForgetPwdResult(result: String)
    }

    interface Presenter : IBasePresenter<View> {

    }
}