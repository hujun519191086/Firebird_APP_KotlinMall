package com.kotlin.user.data.protocol

/*
  注册请求体
 */
data class RegisterReq(val username:String,val password:String,val mobile:String,val Code:String)
