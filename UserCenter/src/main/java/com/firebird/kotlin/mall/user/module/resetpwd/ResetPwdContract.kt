package com.firebird.kotlin.mall.user.module.resetpwd

import com.firebird.kotlin.mall.library.mvp.IBasePresenter
import com.firebird.kotlin.mall.library.mvp.IBaseView

interface ResetPwdContract {
    interface View : IBaseView {
        fun onResetPwdResult(result:String)
    }

    interface Presenter : IBasePresenter<View> {

    }
}