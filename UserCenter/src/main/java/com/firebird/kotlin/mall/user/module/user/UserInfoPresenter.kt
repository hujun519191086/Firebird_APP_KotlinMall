package com.firebird.kotlin.mall.user.module.user

import com.kotlin.user.data.protocol.LoginResultResponse
import com.firebird.kotlin.mall.library.ext.excute
import com.firebird.kotlin.mall.library.mvp.presenter.BasePresenter
import com.firebird.kotlin.mall.library.rx.BaseSubscriber
import com.firebird.kotlin.mall.user.service.UploadService
import com.firebird.kotlin.mall.user.service.UserService
import javax.inject.Inject

/*
  编辑用户资料Presenter
 */
class UserInfoPresenter @Inject constructor() : BasePresenter<UserInfoContract.View>(), UserInfoContract.Presenter {

    @Inject
    lateinit var userService: UserService

    @Inject
    lateinit var uploadService: UploadService

    /*
        获取七牛云上传凭证
     */
    fun getUploadToken() {
        if (!checkNetWork())
            return

        getView().showLoading()
        uploadService.getUploadToken().excute(object : BaseSubscriber<String>(getView()) {
            override fun onNext(t: String) {
                getView().onGetUploadTokenResult(t)
            }
        }, mLifecycleProvider)
    }

    /*
      编辑用户资料
     */
    fun editUser(userIcon: String, userName: String, userGender: String, userSign: String) {
        if (!checkNetWork())
            return

        getView().showLoading()
        userService.editUser(userIcon, userName, userGender, userSign).excute(object : BaseSubscriber<LoginResultResponse>(getView()) {
            override fun onNext(t: LoginResultResponse) {
                getView().onEditUserResult(t)
            }
        }, mLifecycleProvider)
    }
}
