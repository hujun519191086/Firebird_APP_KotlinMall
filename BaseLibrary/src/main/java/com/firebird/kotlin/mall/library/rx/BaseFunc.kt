package com.firebird.kotlin.mall.library.rx

import com.kotlin.base.common.ResultCode
import com.firebird.kotlin.mall.library.data.BaseResp
import rx.Observable
import rx.functions.Func1

class BaseFunc<T> : Func1<BaseResp<T>, Observable<T>> {
    override fun call(t: BaseResp<T>): Observable<T> {
        if (t.errno != ResultCode.SUCCESS) {
            return Observable.error(BaseException(t.errno, t.errmsg))
        }

        return Observable.just(t.data)
    }
}
