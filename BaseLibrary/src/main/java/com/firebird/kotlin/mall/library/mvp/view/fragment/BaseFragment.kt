package com.firebird.kotlin.mall.library.mvp.view.fragment

import com.trello.rxlifecycle.components.support.RxFragment

open class BaseFragment : RxFragment()