package com.firebird.kotlin.mall.library.data

/*
    能用响应对象
    @status:响应状态码
    @message:响应文字消息
    @data:具体响应业务对象
 */
data class BaseResp<out T>(val errno:Int, val errmsg:String, val data:T)
