package com.firebird.kotlin.mall.library.util

import android.content.Context
import android.graphics.drawable.Drawable
//import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.firebird.kotlin.mall.library.R

/*
  Glide工具类
 */
object GlideUtils {
    fun loadImage(context: Context, url: String, imageView: ImageView) {
        Glide.with(context).load(url).centerCrop().into(imageView)
    }

    fun loadImageFitCenter(context: Context, url: String, imageView: ImageView) {
        Glide.with(context).load(url).fitCenter().into(imageView)
    }

    /*
      当fragment或者activity失去焦点或者destroyed的时候，Glide会自动停止加载相关资源，确保资源不会被浪费
     */
    fun loadUrlImage(context: Context, url: String, imageView: ImageView) {
        Glide.with(context)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.icon_back)
                .error(R.drawable.icon_back)
                .centerCrop()
                //.listener(object : RequestListener<Drawable> {
                    /**
                     * 加载失败
                     * @return false 未消费，继续走into(ImageView)
                     *         true 已消费，不再继续走into(ImageView)
                     */
                //    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
               //         return false
               //     }
                    /**
                     * 加载成功
                     * @return false 未消费，继续走into(ImageView)
                     *         true 已消费，不再继续走into(ImageView)
                     */
               //     override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
              ////          return false
              //      }
              //  })
                .into(imageView)
    }
}
