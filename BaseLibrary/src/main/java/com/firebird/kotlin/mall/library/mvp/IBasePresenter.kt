package com.firebird.kotlin.mall.library.mvp

interface IBasePresenter<V : IBaseView> {
    fun onAttach(view: V)

    fun onDetach()
}