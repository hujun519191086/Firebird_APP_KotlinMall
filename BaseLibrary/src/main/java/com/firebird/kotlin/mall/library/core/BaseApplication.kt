package com.firebird.kotlin.mall.library.core

import android.app.Application
import com.alibaba.android.arouter.launcher.ARouter
import com.firebird.kotlin.mall.library.di.component.AppComponent
import com.firebird.kotlin.mall.library.di.component.DaggerAppComponent
import com.firebird.kotlin.mall.library.di.module.AppModule
import com.firebird.kotlin.mall.library.util.AppPrefsUtils

open class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppPrefsUtils.init(this)
        AppStatusTracker.init(this)

        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()

        instance = this

        ARouter.openLog()
        ARouter.openDebug()
        ARouter.init(this)
    }

    companion object {
        lateinit var instance: Application
        lateinit var appComponent: AppComponent
    }
}