package com.firebird.kotlin.mall.core

import cn.jpush.android.api.JPushInterface
import com.firebird.kotlin.mall.library.core.BaseApplication

class MainApplication : BaseApplication() {
    override fun onCreate() {
        super.onCreate()
        JPushInterface.setDebugMode(true)
        JPushInterface.init(this)
    }
}