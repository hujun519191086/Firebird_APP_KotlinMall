package com.firebird.kotlin.mall.provider.common

/*
  业务常量
 */
class ProviderConstant {
    companion object {
        //用户id
        const val KEY_SP_USER_Id= "sp_user_id"
        //用户名称
        const val KEY_SP_USER_NAME = "sp_user_name"
        const val KEY_SP_USER_NICENAME = "sp_user_nicename"
        //用户电话
        const val KEY_SP_USER_MOBILE = "sp_user_mobile"
        //用户头像
        const val KEY_SP_USER_AVATAR = "sp_user_avatar"
        //用户性别
        const val KEY_SP_USER_GENDER = "sp_user_gender"
        //订单ID
        const val KEY_ORDER_ID = "order_id"
        //订单价格
        const val KEY_ORDER_PRICE = "order_price"
    }
}
